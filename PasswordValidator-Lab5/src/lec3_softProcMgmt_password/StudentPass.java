/*
 * @author Portia Ocran , 991545021
 * 
 * This class validates passwords and will be developed using the TDD process
 */

package lec3_softProcMgmt_password;

public class StudentPass {

	public static boolean isValidLength(String password) {

		if (password != null) {
			return password.trim().length() >= 8;
		}

		return false;
	}

	public static boolean isValidDigits(String password) {

		int digitCount = 0;

		if (password != null) {
			for (int i = 0; i < password.length(); i++) {
				if (Character.isDigit(password.charAt(i)))
					digitCount++;

				if (digitCount == 2)
					return true;
			}
		}

		return false;
	}

	public static boolean checkUpperLower(String password) {

		return password != null && password.matches("^(?=.*[a-z])(?=.*[A-Z]).+$");
	}
}
