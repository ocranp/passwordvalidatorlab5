/*
 * @author Portia Ocran , 991545021
 * 
 * This class is used to test the student password validator
 */

package lec3_softProcMgmt_password;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import org.junit.jupiter.api.Test;

class StudentPassTest {

	@Test
	void testisValidLengthRegular() {
		assertTrue("Invalid Password Length", StudentPass.isValidLength("123456789"));
	}

	@Test
	void testisValidLengthException() {
		assertFalse("Invalid Password Length", StudentPass.isValidLength(null));
	}

	@Test
	void testisValidLengthBoundaryIn() {
		assertTrue("Invalid Password Length", StudentPass.isValidLength("12345678"));
	}

	@Test
	void testisValidLengthBoundaryOut() {
		assertFalse("Invalid Password Length", StudentPass.isValidLength("1234567"));
	}

	@Test
	void testisValidDigitsRegular() {
		assertTrue("Invalid Password Length", StudentPass.isValidDigits("test123"));
	}
	
	@Test
	void testisValidDigitsException() {
		assertFalse("Invalid Password Length", StudentPass.isValidDigits(null));
	}
	
	@Test
	void testisValidDigitsBoundaryIn() {
		assertTrue("Invalid Password Length", StudentPass.isValidDigits("test13"));
	}
	
	@Test
	void testisValidDigitsBoundaryOut() {
		assertFalse("Invalid Password Length", StudentPass.isValidDigits("test1"));
	}
	
	@Test
	public void testHasValidCaseCharsRegular() {
		assertTrue("Password must contain uppercase and lowercase characters.", StudentPass.checkUpperLower("Test"));
	}
	
	@Test
	public void testHasValidCaseCharsExceptionBlank() {
		assertFalse("Password must contain uppercase and lowercase characters.", StudentPass.checkUpperLower(""));
	}

	@Test
	public void testHasValidCaseCharsExceptionNull() {
		assertFalse("Password must contain uppercase and lowercase characters.", StudentPass.checkUpperLower(null));
	}
	
	@Test
	public void testHasValidCaseCharsBoundaryOutUpper() {
		assertFalse("Password must contain uppercase and lowercase characters.", StudentPass.checkUpperLower("TEST"));
	}
	
	@Test
	public void testHasValidCaseCharsBoundaryOutLower() {
		assertFalse("Password must contain uppercase and lowercase characters.", StudentPass.checkUpperLower("test"));
	}
	
	@Test
	public void testHasValidCaseCharsBoundaryIn() {
		assertTrue("Password must contain uppercase and lowercase characters.", StudentPass.checkUpperLower("Hi"));
	}

}
